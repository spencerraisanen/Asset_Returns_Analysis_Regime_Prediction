This project involved an attempt to identify regime changes in stock market daily asset returns via the usage of Hidden Markov Models.

The data utilized was the French stock index the SBF250, which had many points of missing data.

For educational purposes, different methods were applied in order to impute the missing data points although this imputationis not necessarily needed or desirable. 

One method involved an application of the Expectation-Maximization Algorithm and the other the Iterative PCA Algorithm.

Given that both methods preserve the covariance structure of the data, they give quite similar results.

Gaussian Hidden Markov Models were applied (as the data was assumed to carry a Gaussian structure).

It appears that the application of a GHMM is able to points of increasing variability, which may or may not lead into a boom or a crash.